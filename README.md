# Deep Learning Examples
This is a repository of some examples in Deep Learning implemented in PyTorch/TensorFlow.
Some tasks(30%) are from the assignment of the course: Deep Learning SoSe 2020 by Prof. Dr. Frank Noé.

Contact info: yuxuan.chen@fu-berlin.de

# Table of Content

## [1. Basic](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/tree/master/01-Basic)
#### 1.1 Image Classification with Logistic Regression
Logistic regression on a subset of MNIST dataset containing 20000 samples. Implemented in: 
- [PyTorch](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_regression_pytorch.ipynb): 
91.00% validation accuracy
- [TensorFlow](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/01-Basic/image_classfication_with_regression_tf.ipynb):
90.65% validation accuracy

#### 1.2 Image Classification with Neural Network
A simple neural network of one hidden layer on the same dataset. Implemented in:
- [PyTorch](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_neural_network_pytorch.ipynb):
96.75% validation accuracy
- [TensorFlow](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_neural_network_tf.ipynb)
96.75% validation accuracy

#### 1.3 Image Classification with CNN
CNN of 2 convolutional layers on the same dataset. Implemented in: 
- [PyTorch](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_CNN_pytorch.ipynb):
98.55% validation accuracy
- [TensorFlow](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/01-Basic/image_classification_with_CNN_tf.ipynb):
98.85% validation accuracy

## [2. Intermediate](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/tree/master/02-Intermediate)
#### 2.1 Sequence Classification with RNN
Use RNN and its variations to classify DNA sequences. Implemented in: 
- [PyTorch](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/02-Intermediate/sequence_classification_with_RNN_pytorch.ipynb): 
100% validation accuracy
- [TensorFlow](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/02-Intermediate/sequence_classification_with_RNN_tf.ipynb):
100% validation accuracy
#### 2.2 VAE, CVAE
We use VAE and CVAE of 2-dim latent space to generate pictures learnt from 20000-sized MNIST dataset. Implemented in:
- [PyTorch](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/02-Intermediate/vae_pytorch.ipynb)
- [TensorFlow](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/02-Intermediate/vae_tf.ipynb)
#### 2.3 Transfer Learning for Image Classification
We use a pre-trained model(VGG16) to predict age and gender. Implemented in:
- [PyTorch](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/02-Intermediate/transfer_learning_with_vgg_for_prediction_pytorch.ipynb)
- [TensorFlow](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/02-Intermediate/transfer_learning_with_vgg_for_prediction_tf.ipynb)

## [3. Advanced](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/tree/master/3.%20Advanced)
#### 3.1 [GAN, CGAN](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/03-Advanced/gan.ipynb)
We use vanilla and conditional GAN of 32-dim latent space to generate from 60000-sized MNIST dataset.
#### 3.2 [Language Model](https://gitlab.com/yuxuan.chen/deep-learning-examples/-/blob/master/03-Advanced/language_models.ipynb)
We use RNN and its variations to train language models over Penntree Bank dataset and do text generation.


## Dependencies
- **Python 3.5+**
- **PyTorch 1.5.0**
- **TensorFlow 2.2.0**
